package Object;

import java.io.Serializable;
import java.net.Socket;
import java.util.Date;

public class User implements Serializable {
	private String email;
	private String username;
	private String password;
	private String displayName;
	private String status;

	public User() {
		// TODO Auto-generated constructor stub
		this.email = "";
		this.username = "";
		this.password = "";
		this.displayName = "";
		this.status = "";

	}

	public User(String email, String username, String password, String displayName, String status) {
		super();
		this.email = email;
		this.username = username;
		this.password = password;
		this.displayName = displayName;
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String toString() {
		String S = this.email+"#"+this.username+"#"+this.password+"#"+this.displayName;
		return S;
	}
	
}
