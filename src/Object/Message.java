package Object;

import java.io.Serializable;

public class Message implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1748174694008707875L;
	private String content;
	private String sender;
	private String receiver;
	private String sentTime;
	private String type;

	public Message(String type) {
		this.type = type;
	}

	public Message(String content, String sender, String receiver, String sentTime, String type) {
		super();
		this.content = content;
		this.sender = sender;
		this.receiver = receiver;
		this.sentTime = sentTime;
		this.type = type;
	}

	public Message() {
		// TODO Auto-generated constructor stub
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getSentTime() {
		return sentTime;
	}

	public void setSentTime(String sentTime) {
		this.sentTime = sentTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
