package Controller.ThreadHandle;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import Controller.Server;
import JBDC.MessageDAO;
import Object.FileInfo;
import Object.Message;
import Object.User;
import View.ServerGUI;

public class ServerThreadHandle extends Thread {

	final static Integer ONLINE = 1;
	final static Integer OFFLINE = 0;

	private Socket clientSocket;
	private Server server;
	private String userName;
	private User user;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private boolean isLogout = false;
	Message msg;

	public ServerThreadHandle(User A, Socket clientSocket, ObjectInputStream serverInput, Server server)
			throws IOException {
		// TODO Auto-generated constructor stub
		this.server = server;
		this.clientSocket = clientSocket;
		this.user = A;
		this.input = serverInput;
		this.output = new ObjectOutputStream(clientSocket.getOutputStream());
		this.input = new ObjectInputStream(clientSocket.getInputStream());
		System.out.println("New connection with client #" + A);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (isLogout != true) {
			try {
				handleClientSocket();
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				isLogout = true;
//				e.printStackTrace();
			}
		}
	}

	private void handleClientSocket() throws ClassNotFoundException, IOException {

		Message message = (Message) this.input.readObject();
		if (message.getType().equals("message")) {
			handleMessage(message);
		} else if (message.getType().equals("logout")) {
			this.output.close();
			this.input.close();
			handleLogout();
		} else if (message.getType().equalsIgnoreCase("listOldMessages")) {
			sendListOldMsg(message.getSender(), message.getReceiver());
		} else if(message.getType().equals("file")) {
			sendFile(msg);
		}
	}

	private synchronized void sendFile(Message msg) throws ClassNotFoundException, IOException {
		BufferedOutputStream bos = null;
		FileInfo fileInfo = (FileInfo)input.readObject();
		File file = new File(fileInfo.getDestinationDirectory()+fileInfo.getFileName());
		
		File checkDir = new File(fileInfo.getDestinationDirectory());
		if(!checkDir.exists()) {
			checkDir.mkdirs();
		}else {
			try {
				bos = new BufferedOutputStream(new FileOutputStream(file, false));
				bos.write(fileInfo.getDataBytes());
				bos.flush();
			}
			finally {
				// TODO: handle finally clause
				bos.close();
			}
		}
	}

	private void handleMessage(Message msg) {
		ServerThreadHandle receiver = server.getListUser().get(msg.getReceiver());
		MessageDAO.insertNewMsg(msg);
		try {
			receiver.sendMessage(msg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println("Reiceiver offline: " + msg.getReceiver());
		}
	}

	public synchronized void sendMessage(Message msg) throws IOException {

		this.output.writeObject(msg);
	}

	public void handleLogout() throws IOException {
		HashMap<String, ServerThreadHandle> newList = server.getListUser();
		newList.replace(this.user.getUsername(), null);
		server.setListUser(newList);
		clientSocket.close();
		ServerGUI.printMessage("User: " + this.user.getUsername() + " --> LOGOUT!");
		server.sendOnlineNoti();
		System.out.println(newList);
	}

//	public void sendMessage(Message msg) throws IOException {
//		
//		receiver.sendMessage(msg);
//	}

	public void sendListUserOnline() throws IOException {
		HashMap<String, ServerThreadHandle> newList = server.getListUser();
		StringBuilder listOnline = new StringBuilder();
		for (Map.Entry A : newList.entrySet()) {
			if (A.getValue() != null) {
				listOnline.append((String) A.getKey() + "#");
			}
		}
		Message msg = new Message("update");
		msg.setContent(listOnline.toString());
		this.output.writeObject(msg);
	}

	private synchronized void sendListOldMsg(String sender, String receiver) throws IOException {
		ArrayList<Message> listMsg = MessageDAO.getAllMessageOfUser(sender, receiver);
		this.output.writeObject(listMsg);
	}

}
