package Controller;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import Object.FileInfo;
import Object.Message;
import View.ChatPanel;
import View.ClientGUI;

public class Client {
	public static HashMap<String, String> listOnlineClient;
	private int timeout = 10000;
	private Socket socket;
	private String username;
	private boolean isLogout = false;
	private ClientGUI clientGUI;
	ObjectInputStream input;
	ObjectOutputStream output;
	private HashMap<String, ChatPanel> listChatPanel;

	public Client(Socket socket, String username, ClientGUI clientGUI) throws IOException {
		this.socket = socket;
		this.username = username;
		this.clientGUI = clientGUI;
		this.output = new ObjectOutputStream(socket.getOutputStream());
		this.input = new ObjectInputStream(socket.getInputStream());

//		UpdateListFriend threadUpdate = new UpdateListFriend(this.username);
//		threadUpdate.start();

		ClientThreadHandle threadReadMsg = new ClientThreadHandle(this.username, clientGUI);
		threadReadMsg.start();
	}

	class ClientThreadHandle extends Thread {
		String username;
		ClientGUI clientGUI;
		public ClientThreadHandle(String username, ClientGUI clientGUI) {
			this.username = username;
			this.clientGUI = clientGUI;
			
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				while (!isLogout) {
					
//		 			Message msg = (Message) input.readObject();
//					
//					if (msg.getType().equals("message")) {
//						listChatPanel = clientGUI.getListChatPanel();
////						System.out.println(listChatPanel);
//						ChatPanel panelReceiver = listChatPanel.get(msg.getSender());			
////						System.out.println(panelReceiver);
////						System.err.println(msg.getContent() + " # " + msg.getSender() + " -> " + msg.getReceiver());
//						panelReceiver.printMessage(msg);
//						
//					} else if (msg.getType().equals("update")) {
//						String[] listOnline = msg.getContent().split("#");
//						clientGUI.resetListFriend();
//						for (String username : listOnline) {
//							if (!username.equals(this.username)) {
//								clientGUI.updateFriendClientGui(username);
//							}
//						}
//					} else {
//						ArrayList<Message> listMsg = (ArrayList<Message>)input.readObject();
//						System.out.println();
//						listChatPanel = clientGUI.getListChatPanel();
//						ChatPanel panelReceiver = listChatPanel.get(msg.getSender());
//						panelReceiver.printOldMgs(listMsg);
//					}
					Object obj = input.readObject();
					
					if(obj instanceof Message) {
			 			Message msg = (Message)obj;
						
						if (msg.getType().equals("message")) {
							listChatPanel = clientGUI.getListChatPanel();
//							System.out.println(listChatPanel);
							ChatPanel panelSender = listChatPanel.get(msg.getSender());	//Nhận tin nhắn từ người gửi đến chatPanel của người đó trong GUI của User hiện tại		
//							System.out.println(panelReceiver);
//							System.err.println(msg.getContent() + " # " + msg.getSender() + " -> " + msg.getReceiver());
							if(panelSender!=null) {
								panelSender.printMessage(msg);
							}
							
						} else if (msg.getType().equals("update")) {
							String[] listOnline = msg.getContent().split("#");
							clientGUI.resetListFriend();
							for (String username : listOnline) {
								if (!username.equals(this.username)) {
									clientGUI.updateFriendClientGui(username);
								}
							}
						}
					} else {
						ArrayList<Message> listMsg = (ArrayList<Message>)obj;
						listChatPanel = clientGUI.getListChatPanel();
						System.out.println(listMsg);
						Message msg = listMsg.get(0);
						listMsg.remove(0);
						ChatPanel panelReceiver = listChatPanel.get(msg.getReceiver());
						panelReceiver.printOldMgs(listMsg);
					}
				}
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public void exit() throws IOException {
		// TODO Auto-generated method stub
		Message msg = new Message("logout");
		output.writeObject(msg);
	}

	public void sendMsg(Message msg) throws IOException {
		output.writeObject(msg);
	}

	public void sendFile(Message msg, File file) throws IOException {
		// TODO Auto-generated method stub
		FileInfo fileInfo = getFileInfo(file);
		output.writeObject(msg);
		output.writeObject(fileInfo);
		
	}
	
	private FileInfo getFileInfo(File file) {
		FileInfo fileInfo = null;
		BufferedInputStream bis = null;
		String destinationDirectory = "D:\\server\\" + username +"\\";;
		System.out.println(destinationDirectory);
		try {
			bis = new BufferedInputStream(new FileInputStream(file));
			fileInfo = new FileInfo();
			byte[] fileBytes = new byte[(int) file.length()];
			bis.read(fileBytes, 0, fileBytes.length);
			fileInfo.setFileName(file.getName());
			fileInfo.setDataBytes(fileBytes);
			fileInfo.setDestinationDirectory(destinationDirectory);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileInfo;
	}
}
