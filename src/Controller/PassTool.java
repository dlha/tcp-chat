package Controller;

import org.mindrot.jbcrypt.*;

public class PassTool {
	public static boolean checkPassword(String pwd, String hashCode) {
		return BCrypt.checkpw(pwd, hashCode);
	}
}
