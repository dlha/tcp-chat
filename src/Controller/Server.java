package Controller;

import java.io.BufferedReader;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import Controller.ThreadHandle.ServerThreadHandle;
import JBDC.UserDAO;
import Object.User;
import View.ServerGUI;

public class Server {

	/*
	 * Khai báo các hằng có tác dụng thông báo trạng thái
	 */

	final static String CREATE_NEW_ACCOUNT_STATUS = "new";
	final static String LOGIN_STATUS = "login";
	final static String UPDATE_ACCOUNT_STATUS = "update";
	final static String SUCSSESFULL = "sucsess";
	final static String FAILURE = "failure";
	final static String ALREADY_LOGIN = "already_login";

	final static String ONLINE = "onl";
	final static String OFFLINE = "off";

	// Khai báo các thuộc tính
	private boolean isStop = false;
	private Thread t;
	private ServerSocket serverSocket;
	private Socket clientSocket = null;
	static HashMap<String, ServerThreadHandle> listUserConnected;
//	private HashMap<String, ServerThreadHandle> listUserConnected;

	public static String getCreateNewAccountStatus() {
		return CREATE_NEW_ACCOUNT_STATUS;
	}

	public static String getLoginStatus() {
		return LOGIN_STATUS;
	}

	public static String getUpdateAccountStatus() {
		return UPDATE_ACCOUNT_STATUS;
	}

	public static String getSucssesfull() {
		return SUCSSESFULL;
	}

	public static String getFailure() {
		return FAILURE;
	}

	public static String getOnline() {
		return ONLINE;
	}

	public static String getOffline() {
		return OFFLINE;
	}

	public static String getAlreadyLogin() {
		return ALREADY_LOGIN;
	}

	public HashMap<String, ServerThreadHandle> getListUser() {
		return listUserConnected;
	}

	public void setListUser(HashMap<String, ServerThreadHandle> listUser) {
		this.listUserConnected = listUser;
	}

	private static ArrayList<ServerThreadHandle> clientList = new ArrayList<ServerThreadHandle>();

	public Server(int port) throws IOException {
		this.serverSocket = new ServerSocket(port);

		System.out.println("Start, Server is running...");
		ServerGUI.printMessage("Server started at port: " + port);
		(new WaitingForConnect()).start();
		initListUser();
	}

	private boolean isExistingAccount(User A) {
		return false;
	}

	private void createClientSocket(User A, Socket clientSocket, ObjectInputStream serverInput) throws IOException {
		ServerThreadHandle handle = new ServerThreadHandle(A, clientSocket, serverInput, this);
		listUserConnected.replace(A.getUsername(), handle);
		handle.start();
		sendOnlineNoti();
		System.out.println(listUserConnected);
	}

	private void initListUser() {
		this.listUserConnected = new HashMap<String, ServerThreadHandle>();
		List<User> list = UserDAO.getListAllUser();
		for (User user : list) {
			listUserConnected.put(user.getUsername(), null);
		}
//		System.out.println(listUserConnected);
	}

	private void checkNewConnectionStatus(Socket clientSocket) throws IOException, ClassNotFoundException {

		ObjectInputStream serverInput = new ObjectInputStream(clientSocket.getInputStream());
		User A = (User) serverInput.readObject();

		DataOutputStream serverOutput = new DataOutputStream(clientSocket.getOutputStream());

		if (A.getStatus().equals(LOGIN_STATUS)) { // Nếu status gửi đến Server thực hiện là tiến hành LOGIN
			User B = UserDAO.checkLoginInfomation(A);
//			System.out.println(B.getStatus());

			if ((listUserConnected.get(B.getUsername()) != null)) {
				serverOutput.writeUTF(ALREADY_LOGIN + "#" + B.getUsername());
				return;
			}

			if (B.getStatus().equals(SUCSSESFULL)) {
				serverOutput.writeUTF(SUCSSESFULL + "#" + B.getUsername());

				ServerGUI.printMessage("User: " + B.getUsername() + " login from " + clientSocket);

				createClientSocket(B, clientSocket, serverInput);
			} else {
				serverOutput.writeUTF(FAILURE + "#" + A.getUsername());
				serverInput.close();
				serverOutput.close();
			}
		} else if (A.getStatus().equals(CREATE_NEW_ACCOUNT_STATUS)) { // Nếu status gửi đến Server thực hiện là tiến
																		// hành tạo new Account
			ServerGUI.printMessage("RECEIVED: Request create new Account from " + clientSocket); //
			Boolean valuate = UserDAO.createAccount(A);

			if (valuate == true) {

				initListUser();// Cập nhật lại listAllUser vì có thêm 1 user mới được tạo!

				ServerGUI.printMessage("Created new User :" + A.getUsername() + " from " + clientSocket);
				serverOutput.writeUTF(SUCSSESFULL); // Nếu tạo thành công --> LOGIN --> New clients

			} else {
				serverOutput.writeUTF(FAILURE); // Thất bại
				ServerGUI.printMessage("Create new Account: " + A.getEmail() + "--> DENIED");
			}
		}
	}

	public ArrayList<ServerThreadHandle> getClientList() {
		return clientList;
	}

	public class WaitingForConnect extends Thread {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while (isStop != true) {
				try {
					clientSocket = serverSocket.accept();
//					System.out.println(clientSocket);
					checkNewConnectionStatus(clientSocket);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					ServerGUI.printMessage("Disconnected from client");
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public synchronized void terminate() throws IOException {
		isStop = true;
		serverSocket.close();
		for (Map.Entry A : listUserConnected.entrySet()) {
			if (A.getValue() != null) {
				ServerThreadHandle T = (ServerThreadHandle) A.getValue();
				T.handleLogout();
			}
		}
		ServerGUI.printMessage("SUCSSES: Server closed!");
	}

	public void sendOnlineNoti() throws IOException {
		if (!isStop) {
			for (Map.Entry A : listUserConnected.entrySet()) {
				if (A.getValue() != null) {
					ServerThreadHandle T = (ServerThreadHandle) A.getValue();
					T.sendListUserOnline();
				}
			}
		}

	}

}