package JBDC;

import java.sql.Connection;
import org.mindrot.jbcrypt.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import Controller.Server;
import Object.Message;
import Object.User;

public class UserDAO {
	private static Connection conn = DBConnection.getConnection();

	public static List<User> getListAllUser() {
//		Connection conn = DBConnection.getConnection();
		String sql = "SELECT * FROM ACCOUNT";
		List<User> list = new ArrayList<User>();
		try {
//			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			Statement st = conn.createStatement();
			ResultSet rst = st.executeQuery(sql);
			while (rst.next()) {
				User A = new User();
				A.setEmail(rst.getString("EMAIL"));
				A.setUsername(rst.getString("USERNAME"));
				A.setPassword(rst.getString("PASSWORD"));
				A.setDisplayName(rst.getString("DISPLAY_NAME"));
				list.add(A);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public static User checkLoginInfomation(User A) {
//		Connection conn = DBConnection.getConnection();
		String sql = "SELECT * FROM ACCOUNT " + "WHERE USERNAME = ? " + "OR EMAIL = ? ";
		User B = new User();
		B.setStatus(Server.getFailure());
		try {
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, A.getUsername());
			ps.setString(2, A.getEmail());

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				B.setEmail(rs.getString("EMAIL"));
				B.setUsername(rs.getString("USERNAME"));
				B.setPassword(rs.getString("PASSWORD"));
				B.setDisplayName(rs.getString("DISPLAY_NAME"));
				
				String pwd = A.getPassword();
//				System.out.println(A.getPassword());
				String hash = B.getPassword();
				boolean valuate = BCrypt.checkpw(pwd, hash);

				if (valuate == true) {
					B.setStatus(Server.getSucssesfull());
					return B;
				} else {
					B = new User();
					B.setStatus(Server.getFailure());
					return B;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return B;
	}
	
	private static boolean isAccountExsited(User A) {
		String sql = "SELECT * FROM ACCOUNT " + "WHERE USERNAME = ? " + "OR EMAIL = ? ";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, A.getUsername());
			ps.setString(2, A.getEmail());
			
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return false;
	}
	
	public static boolean createAccount(User A) {
		if(isAccountExsited(A)) {
			return false;
		}
		String sql = "INSERT INTO ACCOUNT(EMAIL,USERNAME, PASSWORD, DISPLAY_NAME)" + " values(?, ?, ?, ?)";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			
			String pwd = BCrypt.hashpw(A.getPassword(), BCrypt.gensalt(4));

			ps.setString(1, A.getEmail());
			ps.setString(2, A.getUsername());
			ps.setString(3, pwd);
			ps.setString(4, A.getDisplayName());

			ps.execute();
			return true;

		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

	public static boolean updateAccount(User A) {
		String sql = "UPDATE ACCOUNT SET EMAIL = ?, " + "PASSWORD = ?, " + "DISPLAY_NAME = ? " + "WHERE USERNAME = ?";

		try {
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, A.getEmail());
			ps.setString(2, A.getPassword());
			ps.setString(3, A.getDisplayName());

			ps.setString(4, A.getUsername());

			return ps.execute();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return false;
	}
	
	public ArrayList<Message> getAllMessageOfUser(User A, User B) {
		ArrayList<Message> list = new ArrayList<Message>();
		String sql = "SELECT M.ID, M.CONTENT, A.USERNAME AS 'SENDER', M.TYPE, M.SENDED_DATE FROM MESSAGES M inner join ACCOUNT A\r\n" + 
				"ON M.SENDER_ID = A.ID\r\n" + 
				"WHERE (SENDER_ID =(SELECT ID FROM ACCOUNT WHERE USERNAME LIKE '?') and  RECEIVER_ID=(SELECT ID FROM ACCOUNT WHERE USERNAME LIKE '?')) or (SENDER_ID =(SELECT ID FROM ACCOUNT WHERE USERNAME LIKE '?') and RECEIVER_ID=(SELECT ID FROM ACCOUNT WHERE USERNAME LIKE '?'))\r\n" + 
				"ORDER BY ID ASC";
		
		try {
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, A.getUsername());
			ps.setString(2, B.getUsername());
			ps.setString(3, B.getUsername());
			ps.setString(4, A.getUsername());

			ResultSet rst = ps.executeQuery();
			while(rst.next()) {
				Message msg = new Message();
				msg.setSender(rst.getString("SENDER"));
				if(rst.getString("SENDER").equalsIgnoreCase(A.getUsername())) {
					msg.setReceiver(B.getUsername());
				} else {
					msg.setReceiver(A.getUsername());
				}
				msg.setType(rst.getString("TYPE"));
				msg.setContent(rst.getNString("CONTENT"));
				list.add(msg);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}
	
	public static void main(String[] args) {
		User A = new User();
		A.setEmail("dlha.19it1@sict.udn.vn");
		A.setUsername("dlha");
		System.out.println(isAccountExsited(A));
	}
}
