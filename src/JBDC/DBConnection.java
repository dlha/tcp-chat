package JBDC;

import java.sql.Connection;
import java.sql.DriverManager;
/*
 * Lớp dùng để tạo kết nối đến SQL
 */
public class DBConnection {
	public static Connection getConnection() {
		Connection conn = null;
		try {
			// Nạp driver sql server
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String url = "jdbc:sqlserver://localhost:1433;databaseName=ChatTCP;integratedSecurity=true";
			conn = DriverManager.getConnection(url);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return conn;
	}

	public static void main(String[] args) {
		System.out.println(getConnection());
	}
}
