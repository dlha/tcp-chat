package JBDC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Object.Message;
import Object.User;

public class MessageDAO {
	private static Connection conn = DBConnection.getConnection();
	
	public static ArrayList<Message> getAllMessageOfUser(String sender, String receiver) {
		ArrayList<Message> list = new ArrayList<Message>();
		Message noti = new Message("listOldMessages");
		noti.setSender(sender);
		noti.setReceiver(receiver);
		list.add(noti);
		String sql = "SELECT M.ID, M.CONTENT, A.USERNAME AS 'SENDER', M.TYPE, M.SENDED_DATE FROM MESSAGES M inner join ACCOUNT A\r\n" + 
				"ON M.SENDER_ID = A.ID\r\n" + 
				"WHERE (SENDER_ID =(SELECT ID FROM ACCOUNT WHERE USERNAME LIKE ?) and  RECEIVER_ID=(SELECT ID FROM ACCOUNT WHERE USERNAME LIKE ?)) or (SENDER_ID =(SELECT ID FROM ACCOUNT WHERE USERNAME LIKE ?) and RECEIVER_ID=(SELECT ID FROM ACCOUNT WHERE USERNAME LIKE ?))\r\n" + 
				"ORDER BY ID ASC";
		
		try {
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, sender);
			ps.setString(2, receiver);
			ps.setString(3, receiver);
			ps.setString(4, sender);

			ResultSet rst = ps.executeQuery();
			while(rst.next()) {
				Message msg = new Message();
				msg.setSender(rst.getString("SENDER"));
				if(rst.getString("SENDER").equalsIgnoreCase(sender)) {
					msg.setReceiver(receiver);
				} else {
					msg.setReceiver(sender);
				}
				msg.setType(rst.getString("TYPE"));
				msg.setContent(rst.getNString("CONTENT"));
				list.add(msg);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}
	
	public static int insertNewMsg(Message msg) {
		int n  = 0;
		String sql = "INSERT INTO MESSAGES(CONTENT, SENDER_ID, RECEIVER_ID, TYPE, SENDED_DATE) \r\n" + 
				"VALUES (?, (SELECT ID FROM ACCOUNT WHERE USERNAME LIKE ?), (SELECT ID FROM ACCOUNT WHERE USERNAME LIKE ?), ?, '123123123')";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, msg.getContent());
			ps.setString(2, msg.getSender());
			ps.setString(3, msg.getReceiver());
			ps.setString(4, msg.getType());

			n= ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return n;
	}
	
	public static void main(String[] args) {
		ArrayList<Message> list = new MessageDAO().getAllMessageOfUser("dlha", "thang");
		System.out.println(list);
	}
}
