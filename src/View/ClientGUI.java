package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controller.Client;
import Object.Message;

import java.awt.Dimension;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import java.awt.CardLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ClientGUI extends JFrame {

	private JPanel contentPane;
	private Client clientNode;
	private JList listFriend;
	private String username;
	private JPanel panelChatGUI;
	private CardLayout cardLayout;
	static DefaultListModel<String> model = new DefaultListModel<String>();
	public HashMap<String, ChatPanel> listChatPanel = new HashMap<String, ChatPanel>();

	public void setListChatPanel(HashMap<String, ChatPanel> listChatPanel) {
		this.listChatPanel = listChatPanel;
	}

	public HashMap<String, ChatPanel> getListChatPanel() {
		return listChatPanel;
	}

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 * 
	 * @param clientSocket
	 * @param clientOutput
	 * @throws IOException
	 */
	public ClientGUI(Socket clientSocket, String username) throws IOException {
		this.username = username;

		clientNode = new Client(clientSocket, username, this);

		this.setTitle(username);

		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				try {
					clientNode.exit();
					System.exit(0);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		setBounds(100, 100, 1000, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panelActiveUser = new JPanel();
		panelActiveUser.setPreferredSize(new Dimension(150, 10));
		contentPane.add(panelActiveUser, BorderLayout.WEST);
		panelActiveUser.setLayout(new BorderLayout(0, 0));

		JPanel panelMain = new JPanel();
		contentPane.add(panelMain, BorderLayout.CENTER);
		panelMain.setLayout(new BorderLayout(0, 0));

		panelChatGUI = new JPanel();
		panelMain.add(panelChatGUI, BorderLayout.CENTER);
		panelChatGUI.setLayout(new CardLayout(0, 0));

		listFriend = new JList<String>(model);
		listFriend.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String receiver = listFriend.getSelectedValue().toString();

				if (!listChatPanel.containsKey(receiver)) {
//					ChatPanel panel = new ChatPanel(receiver, username, clientNode);
//					panelChatGUI.add(panel, receiver);
//					cardLayout = (CardLayout) panelChatGUI.getLayout();
//					cardLayout.show(panelChatGUI, receiver);
//		 			listChatPanel.put(receiver, panel);
//					panelChatGUI.validate();
//					panelChatGUI.repaint();
					try {
						createChatPanel(receiver);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					System.out.println(listChatPanel);
				} else {
					cardLayout = (CardLayout) panelChatGUI.getLayout();
					cardLayout.show(panelChatGUI, receiver);
				}

			}
		});
		panelActiveUser.add(listFriend);
		this.setVisible(true);
	}

	public static void resetListFriend() {
		// TODO Auto-generated method stub
		model.clear();
	}

	public void updateFriendClientGui(String username) throws IOException {
		// TODO Auto-generated method stub
		model.addElement(username);
//		createChatPanel(username);
	}

	private void createChatPanel(String username) throws IOException {
		// TODO Auto-generated method stub
		ChatPanel panel = new ChatPanel(username, this.username , this.clientNode);
		listChatPanel.put(username, panel);
		panelChatGUI.add(panel, username);
		
		Message msg = new Message("listOldMessages");
		
		msg.setSender(this.username);
		msg.setReceiver(username);
		
		clientNode.sendMsg(msg);
		
		panelChatGUI.validate();
		panelChatGUI.repaint();
	}

}
