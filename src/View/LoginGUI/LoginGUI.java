package View.LoginGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;

public class LoginGUI extends JFrame {

	JPanel contentPane;
	CardLayout card;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginGUI frame = new LoginGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 490);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		card = new CardLayout(0, 0);
		contentPane.setLayout(card);
		contentPane.add(new LoginPanel(this), "LOGIN");
		contentPane.add(new RegisterPanel(this), "REGISTER");
	}
	
	public void switchPanel() {
		card.next(contentPane);
	}
	
	public void showErrorDialog(String msg) {
		JOptionPane.showMessageDialog(contentPane, msg, "Lỗi", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showSucsessDialog(String msg) {
		JOptionPane.showMessageDialog(contentPane, msg, "Thành công", JOptionPane.DEFAULT_OPTION);
	}
	
	public int showConfirmDialog(String msg) {
		int check = JOptionPane.showConfirmDialog(contentPane, msg, "Xác nhận", JOptionPane.YES_NO_OPTION);
		return check;
	}
}
