package View.LoginGUI;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import Controller.Server;
import Object.User;
import View.ClientGUI;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.awt.event.ActionEvent;

public class LoginPanel extends JPanel {
	private JTextField textField;
	private JPasswordField passwordField;
	private Socket clientSocket;
	LoginGUI mainGUI;

	/**
	 * Create the panel.
	 */
	public LoginPanel(LoginGUI loginGUI) {
		setLayout(null);
		mainGUI = loginGUI;
		JLabel lblNewLabel = new JLabel("LOGIN");
		lblNewLabel.setFont(new Font("Segoe UI", Font.BOLD, 42));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(142, 56, 300, 87);
		add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Tài khoản / Email");
		lblNewLabel_1.setBounds(68, 187, 94, 31);
		add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Mật khẩu");
		lblNewLabel_1_1.setBounds(68, 254, 94, 31);
		add(lblNewLabel_1_1);
		
		textField = new JTextField();
		textField.setBounds(174, 187, 300, 31);
		add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(174, 254, 300, 31);
		add(passwordField);
		
		JButton btnNewButton = new JButton("Login");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				User A = new User();
				if(textField.getText().isEmpty() || passwordField.getPassword().length==0) {
					mainGUI.showErrorDialog("Tài khoản / Mật khẩu không được để trống!");
					return;
				}
				boolean checkEmail = textField.getText().toString().matches("^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$");
				
				if(checkEmail) {
					A.setEmail(textField.getText().trim());
				}else A.setUsername(textField.getText().trim());
				
				String pwd = String.valueOf(passwordField.getPassword());
				
				System.out.println(pwd);
				A.setPassword(pwd);
				A.setStatus(Server.getLoginStatus());
				
				try {
					clientSocket = new Socket("localhost", 8080);
					ObjectOutputStream clientOutput = new ObjectOutputStream(clientSocket.getOutputStream());
//					DataInputStream clientInput = new DataInputStream(clientSocket.getInputStream());
					
					clientOutput.writeObject(A);
					
					DataInputStream clientInput = new DataInputStream(clientSocket.getInputStream());
//					ObjectInputStream clientInput = new ObjectInputStream(clientSocket.getInputStream());

					String M = (String)clientInput.readUTF();
					String[] msg = M.split("#");
					A.setUsername(msg[1]);
					
					if(msg[0].equals(Server.getSucssesfull())) {
						mainGUI.showSucsessDialog("Đăng nhập thành công!");
			 			new ClientGUI(clientSocket, A.getUsername());
	 					mainGUI.dispose();
	 					
		 			}else if(msg[0].equals(Server.getAlreadyLogin())){
						mainGUI.showErrorDialog("Người dùng hiện đã đăng nhập!");
						textField.setText("");
						passwordField.setText("");
					} else {
						mainGUI.showErrorDialog("Tên đăng nhập hoặc mật khẩu không đúng !");
						textField.setText("");
						passwordField.setText("");
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					mainGUI.showErrorDialog("Không thể kết nối đến server");
				}
				
			}
		});
		btnNewButton.setBounds(248, 347, 89, 23);
		add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Tạo tài khoản mới");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainGUI.switchPanel();
			}
		});
		btnNewButton_1.setBounds(427, 415, 161, 23);
		add(btnNewButton_1);

	}
}
