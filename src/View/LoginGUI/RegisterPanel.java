package View.LoginGUI;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import Controller.Server;
import Object.User;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.awt.event.ActionEvent;

public class RegisterPanel extends JPanel {
	private JTextField tfUsername;
	private JPasswordField passwordField;
	private JTextField tfEmail;
	private JTextField tfDisplayName;
	private JPasswordField passwordFieldRepeat;
	private Socket clientSocket;
	final String regex = "^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$";
	LoginGUI loginGUI;

	/**
	 * Create the panel.
	 * @param loginGUI 
	 */
	public RegisterPanel(LoginGUI loginGUI) {
		setLayout(null);
		this.loginGUI = loginGUI;
		JLabel lblNewLabel = new JLabel("TẠO TÀI KHOẢN");
		lblNewLabel.setFont(new Font("Segoe UI", Font.BOLD, 42));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(103, 25, 393, 87);
		add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Tên đăng nhập");
		lblNewLabel_1.setToolTipText("");
		lblNewLabel_1.setBounds(68, 123, 94, 31);
		add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Mật khẩu");
		lblNewLabel_1_1.setBounds(68, 272, 94, 31);
		add(lblNewLabel_1_1);
		
		tfUsername = new JTextField();
		tfUsername.setToolTipText("Dài không quá 16 kí tự, viết liền không dấu và không chứa các kí tự đặc biệt.");
		tfUsername.setBounds(174, 123, 300, 31);
		add(tfUsername);
		tfUsername.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(174, 272, 300, 31);
		add(passwordField);
		
		JLabel lblNewLabel_1_2 = new JLabel("Email");
		lblNewLabel_1_2.setBounds(68, 176, 94, 31);
		add(lblNewLabel_1_2);
		
		tfEmail = new JTextField();
		tfEmail.setColumns(10);
		tfEmail.setBounds(174, 176, 300, 31);
		add(tfEmail);
		
		JLabel lblNewLabel_1_3 = new JLabel("Tên hiển thị");
		lblNewLabel_1_3.setBounds(68, 224, 94, 31);
		add(lblNewLabel_1_3);
		
		tfDisplayName = new JTextField();
		tfDisplayName.setColumns(10);
		tfDisplayName.setBounds(174, 224, 300, 31);
		add(tfDisplayName);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Nhập lại mật khẩu");
		lblNewLabel_1_1_1.setBounds(68, 322, 94, 31);
		add(lblNewLabel_1_1_1);
		
		passwordFieldRepeat = new JPasswordField();
		passwordFieldRepeat.setBounds(174, 322, 300, 31);
		add(passwordFieldRepeat);
		
		JButton btnCreate = new JButton("Tạo");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tfUsername.getText().isEmpty() && tfDisplayName.getText().isEmpty() && tfEmail.getText().isEmpty() && passwordField.getPassword().length==0 && passwordFieldRepeat.getPassword().length==0) {
					loginGUI.showErrorDialog("Tất cả các trường là bắt buộc \n Không được để trống!");
					return;
				}
				boolean checkEmail = tfEmail.getText().toString().matches(regex);
				if(!checkEmail) {
					loginGUI.showErrorDialog("Email bạn nhập không đúng");
					return;
				}
				
				if(!String.valueOf(passwordField.getPassword()).equals(String.valueOf(passwordFieldRepeat.getPassword()))) {
					loginGUI.showErrorDialog("Mật khẩu bạn nhập không trùng nhau");
				}
				
				//Khơi tạo object
				User A = new User();
				A.setEmail(tfEmail.getText().toString());
				A.setUsername(tfUsername.getText().trim());
				A.setDisplayName(tfDisplayName.getText().trim());
				A.setPassword(String.valueOf(passwordField.getPassword()));
				A.setStatus(Server.getCreateNewAccountStatus());
				
				try {
					clientSocket = new Socket("localhost", 8080);
					ObjectOutputStream clientOutput = new ObjectOutputStream(clientSocket.getOutputStream());
					
					clientOutput.writeObject(A);
					
					DataInputStream clientInput = new DataInputStream(clientSocket.getInputStream());
//					ObjectInputStream clientInput = new ObjectInputStream(clientSocket.getInputStream());

					String msg = clientInput.readUTF();
					clientInput.close();
					
					if(msg.equals(Server.getSucssesfull())) {
						int confirm = loginGUI.showConfirmDialog("Bạn có muốn đăng nhập ?");
						if(confirm == JOptionPane.YES_OPTION) {
							loginGUI.switchPanel();
						}else {
							
						}
					}else {
						loginGUI.showErrorDialog("Thất bại\nUsername hoặc Email đã tồn tại");
					}
				} catch (Exception e2) {
					// TODO: handle exception
					loginGUI.showErrorDialog("Không thể kết nối đến server \n Kiểm tra kết nối mạng của bạn và thử lại!");
				}
			}
		});
		btnCreate.setBounds(268, 385, 89, 23);
		add(btnCreate);

	}
}
