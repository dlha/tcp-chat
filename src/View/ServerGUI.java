package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controller.Server;

import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class ServerGUI extends JFrame {

	private JPanel contentPane;
	private Server server;
	
	private static JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ServerGUI frame = new ServerGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ServerGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 640, 490);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setPreferredSize(new Dimension(10, 100));
		panel.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(null);
		
		JButton btnStartServer = new JButton("START");
		btnStartServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					server = new Server(8080);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					printMessage("Start error");
				}
				
			}
		});
		btnStartServer.setBounds(435, 11, 153, 33);
		panel_1.add(btnStartServer);
		
		JButton btnStopServer = new JButton("STOP");
		btnStopServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					server.terminate();
			 	} catch (IOException e1) {
					// TODO Auto-generated catch block
					
				}
		 	}
		});
		btnStopServer.setBounds(435, 56, 153, 33);
	 	panel_1.add(btnStopServer);
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);
		
		textArea = new JTextArea("Welcome!");
		textArea.setForeground(Color.GREEN);
		textArea.setFont(new Font("Consolas", Font.PLAIN, 15));
		textArea.setLineWrap(true);
		textArea.setBackground(Color.BLACK);
		scrollPane.setViewportView(textArea);
	}
	
	public static void printMessage(String S) {
		textArea.append("\n"+S);
	}

}
