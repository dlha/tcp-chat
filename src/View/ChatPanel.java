package View;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileSystemView;

import Controller.Client;
import Object.FileInfo;
import Object.Message;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;

public class ChatPanel extends JPanel {
	private String receiver;
	private JPanel panelShowMessage;
	private String sender;
	private Client clientNote;
	private final JFileChooser fc;

	/**
	 * Create the panel.
	 * 
	 * @param clientNode
	 * @param username2
	 */
	public ChatPanel(String receiver, String sender, Client clientNode) {
		this.fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		this.sender = sender;
		this.receiver = receiver;
		this.clientNote = clientNode;

		setLayout(new BorderLayout(0, 0));

		JPanel panelInfoReceiver = new JPanel();
		panelInfoReceiver.setPreferredSize(new Dimension(10, 75));
		add(panelInfoReceiver, BorderLayout.NORTH);
		panelInfoReceiver.setLayout(null);

		JLabel lblNewLabel = new JLabel(receiver);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		lblNewLabel.setBounds(26, 11, 174, 53);
		panelInfoReceiver.add(lblNewLabel);

		JPanel panelAction = new JPanel();
		panelAction.setPreferredSize(new Dimension(10, 100));
		add(panelAction, BorderLayout.SOUTH);
		panelAction.setLayout(null);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(29, 11, 602, 78);
		panelAction.add(scrollPane_1);

		JTextArea textArea = new JTextArea();
		scrollPane_1.setViewportView(textArea);

		JButton btnSendMsg = new JButton("Send");
		btnSendMsg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(textArea.getText().isBlank()) return;
				
				Message msg = new Message("message");
				msg.setReceiver(receiver);
				msg.setSender(sender);
				msg.setContent(textArea.getText().toString());

				try {
					clientNode.sendMsg(msg);
					printMessage(msg);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "Mất kết nối đến server", "Lỗi", JOptionPane.ERROR_MESSAGE);
				}
				textArea.setText("");
			}
		});
		btnSendMsg.setBounds(688, 16, 89, 23);
		panelAction.add(btnSendMsg);
		
		JButton btnSendFile = new JButton("send file");
		btnSendFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					sendFile();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSendFile.setBounds(686, 52, 89, 23);
		panelAction.add(btnSendFile);

		JPanel panel = new JPanel();
		add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 815, 354);
		panel.add(scrollPane);

		panelShowMessage = new JPanel();
		scrollPane.setViewportView(panelShowMessage);
		panelShowMessage.setLayout(new BoxLayout(panelShowMessage, BoxLayout.Y_AXIS));
	}

	private void sendFile() throws IOException {
		// TODO Auto-generated method stub
		int cofirm = fc.showOpenDialog(this);
		if(cofirm == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			Message msg = new Message("file");
			msg.setSender(sender);
			msg.setReceiver(receiver);
			clientNote.sendFile(msg, file);
		}
	}

	public void printMessage(Message msg) {
		JLabel newLabel = new JLabel();
		String text = msg.getContent();
		String formatText;
		
		if (msg.getSender().equals(this.sender)) {
			newLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		 	formatText = String.format(
					"<html><div WIDTH=%d style =\"background-color : #ffffff; padding: 10;\">%s</div></html>", 450,
					text);
		} else {
			formatText = String.format(
					"<html><div WIDTH=%d style =\"background-color : #92a8d1; padding: 10;\">%s</div></html>", 450,
					text);
		}

		newLabel.setText(formatText);
		newLabel.setBorder(new EmptyBorder(10, 10, 10, 10));
		panelShowMessage.add(newLabel);
		panelShowMessage.revalidate();
		panelShowMessage.repaint();

		int height = (int) panelShowMessage.getPreferredSize().getHeight();
		Rectangle rect = new Rectangle(0, height, 10, 10);
		panelShowMessage.scrollRectToVisible(rect);
	}

	public void printOldMgs(ArrayList<Message> listMsg) {
		// TODO Auto-generated method stub
		for (Message message : listMsg) {
			printMessage(message);
		}
		
	}
}
